This API was created for me to fiddle around with Node and Express and try working with native Javascript again instead of TypeScript. 
I once more realized how awesome TypeScript is though, so this has probably been my last project without it. 

Take a look at the testing methods I used, because those are pretty nice (I think) 🥳

## As testing it all took too much time, I focussed on a few tests to 'test my skills' instead of full coverage.
This is not a real app. 👍
