module.exports = {
    verbose: false,
    setupFiles: ["./jest.setup.js"],
    setupFilesAfterEnv: ["./jest.runserver.js"],
    reporters: ["jest-silent-reporter"]
}