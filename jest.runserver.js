beforeAll(async () => {
    globalThis.globalServer = await require('./src/app');       
});

afterAll(async () => {
    if(globalThis.globalServer) await globalThis.globalServer.close();
})
