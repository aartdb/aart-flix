const winston = require('winston');

// setup jest to use a separate testing environment
require('dotenv').config({ path: './.env.test'});
