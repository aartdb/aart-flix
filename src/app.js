const express = require('express');
const winston = require('winston');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const app = express();

require('./startup/logging')();
require('./startup/routes')(app);
require('./startup/db')();
require('./startup/config')();
require('./startup/morgan')(app);
require('./startup/prod')(app);

const runServer = async () => {
    let port = process.env.PORT || 3000;
    if(process.env.NODE_ENV === 'test') {
        port = await require('portfinder').getPortPromise({
            port,
            stopPort: port + 1000
        });
    }
    
    return app.listen(port, () => winston.info(`Listening on port ${port}...`));
}

module.exports = runServer();