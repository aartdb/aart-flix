const path = require('path');

module.exports = function(path) {
    const filename = path.match(/([^\.\\]*)\.js/g)[0].split('.')[0];
    globalThis.calledMiddlewares = globalThis.calledMiddlewares || [];

    globalThis.calledMiddlewares.push(filename); 
}