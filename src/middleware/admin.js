
module.exports = function(req, res, next) {
    require("./addCalledMiddleware")(__filename);

    const user = req.user;
    if(!user) return res.status(500).send(`Include authorization middleware to be able to check if it's an admin.`);
    if(!user.isAdmin) return res.status(403).send('Access denied.');

    next();
}