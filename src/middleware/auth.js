const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    require("./addCalledMiddleware")(__filename);
    
    const token = req.header('x-auth-token');
    if(!token) return res.status(401).send('Access denied, no token provided (x-auth-token).');

    try{ 
        const decoded = jwt.verify(token, process.env.JWT_PRIVATE_KEY);
        req.user = decoded;
        next();
    } catch(ex) {
        return res.status(400).send('Invalid token');
    }
}
