const winston = require('winston');

module.exports = function(err, req, res, next) {
    require("./addCalledMiddleware")(__filename);

    // winston doesn't display a stack trace... So let's use console.log
    console.log(err); // it's an error, but white is easier to read than red
    winston.error(err.message, err);

    res.status(500).send('Something failed.');
}