const debug = require('debug')('app:middleware');


function log(req, res, next) {
    require("./addCalledMiddleware")(__filename);
    
    debug('Logging...');
    next();
}

module.exports = log;