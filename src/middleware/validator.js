module.exports = (validator) => {
    
    return (req, res, next) => {
        require("./addCalledMiddleware")(__filename);
        // require("./addCalledMiddleware")(validator);

        const { error } = validator(req.body);
        if(error) return res.status(400).send(new Error(error).message);
        next();
    }
}