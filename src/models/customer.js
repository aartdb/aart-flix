const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 255,
        minlength: 1
    },
    isGold: {
        type: Boolean,
        default: false
    },
    phone: {
        type: String,
        minlength: 6,
        maxlength: 50,
        required: function() { return !this.email; } // require email or phone
    },
    email: {
        type: String,
        minlength: 6,
        maxlength: 50,
        required: function() { return !this.phone; } // require email or phone
    },
    dateCreated: {
        type: Date,
        default: Date.now
    }
});

const Customer = mongoose.model('Customer', customerSchema);

module.exports.Customer = Customer;
module.exports.customerSchema = customerSchema;