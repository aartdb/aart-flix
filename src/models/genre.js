const mongoose = require('mongoose');
const Joi = require('joi');

const genreSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 255,
        minlength: 5
    },
    dateCreated: {
        type: Date,
        default: Date.now
    }
});

const Genre = mongoose.model('Genre', genreSchema);

function validate(genre) {
    const schema = {
        name: Joi.string().max(255).min(5).required()
    }

    return Joi.validate(genre, schema);
}

module.exports.Genre = Genre;
module.exports.genreSchema = genreSchema;
module.exports.validate = validate;