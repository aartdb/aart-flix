const mongoose = require('mongoose');
const Joi = require('joi');
const { genreSchema } = require('./genre');

const movieSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        maxlength: 255,
        minlength: 1,
        trim: true
    },
    genre: {
        type: new mongoose.Schema({ name: genreSchema.tree.name }), // grab only the name from the genre for quick retrieval
        required: true
    },
    numberInStock: {
        type: Number,
        default: 0,
        min: 0,
        max: 10000
    },
    dailyRentalRate: {
        type: Number,
        required: true,
        min: 0,
        max: 10000
    },
    dateCreated: {
        type: Date,
        default: Date.now
    }
});

const Movie = mongoose.model('Movie', movieSchema);

function validate(movie) {
    const schema = {
        title: Joi.string().max(255).min(2).required(),
        genreId: Joi.objectId().required(), // is converted to a genre object
        numberInStock: Joi.number().min(0).max(10000),
        dailyRentalRate: Joi.number().min(0).max(10000).required()
    }

    return Joi.validate(movie, schema);
}

module.exports.Movie = Movie;
module.exports.movieSchema = movieSchema;
module.exports.validate = validate;