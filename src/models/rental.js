const mongoose = require('mongoose');
const Joi = require('joi');
const { customerSchema } = require('../models/customer');
const { movieSchema } = require('../models/movie');
const {differenceInDays} = require('date-fns');

const rentalSchema = new mongoose.Schema({
    dateCreated: {
        type: Date,
        required: true,
        default: Date.now
    },
    customer: {
        type: new mongoose.Schema({ 
            name: customerSchema.tree.name,
            isGold: customerSchema.tree.isGold,
            phone: customerSchema.tree.phone,
            email: customerSchema.tree.email,
        }),
        required: true
    },
    movie: {
        type: new mongoose.Schema({ 
            title: movieSchema.tree.title,
            dailyRentalRate: movieSchema.tree.dailyRentalRate
        }),
        required: true
    },
    dateReturned: {
        type: Date
    },
    rentalFee: {
        type: Number,
        min: 0,
        max: 100000
    }
});

rentalSchema.statics.lookup = function(customerId, movieId) {
    return this.findOne({
        'customer._id': customerId,
        'movie._id': movieId,
    })
}

rentalSchema.methods.return = function() {
    this.dateReturned = new Date(); 

    const daysRented = differenceInDays(this.dateReturned, this.dateCreated);
    this.rentalFee = daysRented * this.movie.dailyRentalRate;
}

const Rental = mongoose.model('Rental', rentalSchema);

function validate(rental) {
    const schema = {
        customerId: Joi.objectId().required(),
        movieId: Joi.objectId().required()
    };

    return Joi.validate(rental, schema);
}
function validateReturn(rentalReturn) {
    // for now, redirect to root validate
    return validate(rentalReturn);
}

exports.Rental = Rental;
exports.validate = validate;
exports.validateReturn = validateReturn;