const express = require('express');
const auth = require('./auth');
const genres = require('./genres');
const customers = require('./customers');
const movies = require('./movies');
const rentals = require('./rentals');
const returns = require('./returns'); 
const users = require('./users');

const router = express.Router();

router.use('/auth', auth);
router.use('/genres', genres);
router.use('/customers', customers);
router.use('/movies', movies);
router.use('/rentals', rentals);
router.use('/returns', returns);
router.use('/users', users);

if(process.env.NODE_ENV === 'test') router.use('/tests', require('./tests'));

module.exports = router;