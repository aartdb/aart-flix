const jwt = require('jsonwebtoken');
const express = require('express');
const Joi = require('joi');
const bcrypt = require('bcrypt');
const { User } = require('../models/user');

const router = express.Router();

router.post('/', async (req, res) => {
    let requester = req.body;
    const {error} = validate(requester);
    if(error) return res.status(400).send(new Error(error).message);

    let user = await User.findOne({ email: requester.email })
    if(!user) return res.status(400).send('Invalid email or password.');

    const validPassword = await bcrypt.compare(requester.password, user.password);
    if(!validPassword) return res.status(400).send('Invalid email or password.');

    const token = user.generateAuthToken();
    
    res.send(token);
});

function validate(req) {
    const schema = {
        email: Joi.string().max(255).email().required(),
        password: Joi.string().min(5).max(1024).required()
    }

    return Joi.validate(req, schema);
}

module.exports = router;