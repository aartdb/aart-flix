const express = require('express');
const { Customer } = require('../models/customer');
const auth = require('../middleware/auth');
const validateObjectId = require('../middleware/validateObjectId');

const router = express.Router();

router.get('/', async (req, res) => {
    const customers = await Customer.find().sort('name');

    res.send(customers);
});

router.post(`/`, auth, async (req, res) => {
    try {
        const customer = new Customer(req.body);
        const createdCustomer = await customer.save();

        res.send(createdCustomer);
    } catch(ex) {
        res.send(ex.message);
    }
});

router.get(`/:id`, validateObjectId, async (req, res) => {
    const customer = await Customer.findById(req.params.id);
    if(!customer) return res.status(404).send(`Couldn't find a customer with that ID`);

    res.send(customer);
});

router.patch(`/:id`, [validateObjectId, auth], async (req, res) => {
    try {
        const customer = await Customer.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        });
        
        if(!customer) return res.status(404).send(`Couldn't find a customer with that ID`);

        res.send(customer);
    } catch(ex) {
        return res.status(400).send(ex.message);
    }
});

router.delete(`/:id`, [validateObjectId, auth], async (req, res) => {
    try {
        const customer = await Customer.findByIdAndRemove(req.params.id);

        if(!customer) res.status(404).send(`Couldn't find a customer with that ID`);

        res.send(customer);
    } catch(ex) {
        return res.status(400).send(ex.message);
    }
});

module.exports = router;