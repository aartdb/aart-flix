const express = require('express');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const { Genre, validate } = require('../models/genre');
const mongoose = require('mongoose');
const validateObjectId = require('../middleware/validateObjectId');
const validator = require('../middleware/validator');

const router = express.Router();


router.get('/', async (req, res) => {
    const genres = await Genre.find().sort('name');

    res.send(genres);
});

router.post(`/`, [auth, admin, validator(validate)], async (req, res) => {
    try {
        const rawGenre = req.body;

        const genre = await new Genre(rawGenre).save();

        res.send(genre);
    } catch(ex) {
        res.status(500).send(ex.message);
    }
});

router.get(`/:id`, validateObjectId, async (req, res) => {
    const id = req.params.id;
    const genre = await Genre.findById(id);
    if(!genre) return res.status(404).send(`Couldn't find a genre with that ID`);

    res.send(genre);
});

router.put(`/:id`, [validateObjectId, auth, admin, validator(validate)], async (req, res) => {
    const genre = await Genre.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });
    
    if(!genre) return res.status(404).send(`Couldn't find a genre with that ID`);

    res.send(genre);
});

router.delete(`/:id`, [validateObjectId, auth, admin], async (req, res) => {
    const genre = await Genre.findByIdAndRemove(req.params.id);

    if(!genre) return res.status(404).send(`Couldn't find a genre with that ID`);

    res.send(genre);
});

module.exports = router;