const express = require('express');
const Joi = require('joi');
const { Movie, validate } = require('../models/movie');
const { Genre } = require('../models/genre');
const auth = require('../middleware/auth');
const validateObjectId = require('../middleware/validateObjectId');
const admin = require('../middleware/admin');
const validator = require('../middleware/validator');

const router = express.Router();

router.get('/', async (req, res) => {
    const movies = await Movie.find().sort('name');

    res.send(movies);
});

router.get(`/:id`, validateObjectId, async (req, res) => {
    const movie = await Movie.findById(req.params.id);
    if(!movie) return res.status(404).send(`Couldn't find a movie with that ID`);

    res.send(movie);
});

router.post(`/`, [auth, admin, validator(validate)], async (req, res) => {
    let movie = req.body;
    const genre = await Genre.findById(movie.genreId);

    if(!genre) return res.status(404).send(`Couldn't find genre with that id`);

    movie = new Movie(movie);
    movie.genre = {
        _id: genre._id,
        name: genre.name
    };

    const createdMovie = await movie.save();

    res.send(createdMovie);
});

router.put(`/:id`, [validateObjectId, auth], async (req, res) => {
    try {
        const movie = await Movie.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        });
        
        if(!movie) return res.status(404).send(`Couldn't find a movie with that ID`);

        res.send(movie);
    } catch(ex) {
        return res.status(400).send(ex.message);
    }
});

router.delete(`/:id`, [validateObjectId, auth], async (req, res) => {
    try {
        const movie = await Movie.findByIdAndRemove(req.params.id);

        if(!movie) res.status(404).send(`Couldn't find a movie with that ID`);

        res.send(movie);
    } catch(ex) {
        return res.status(400).send(ex.message);
    }
});

module.exports = router;