const express = require('express');
const mongoose = require('mongoose');
const { Rental, validate } = require('../models/rental');
const { Customer } = require('../models/customer');
const { Movie } = require('../models/movie');
const Fawn = require('fawn');
const router = express.Router();
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');

Fawn.init(mongoose);

router.get('/', async (req, res) => { 
    const rentals = await Rental.find().sort('-dateCreated');
    res.send(rentals);
});

router.post('/', [auth, validator(validate)], async (req, res) => {
    let rental = req.body;

    const customer = await Customer.findById(rental.customerId);
    if(!customer) return res.status(404).send(`Couldn't find a customer with that ID`);
    rental.customer = {
        _id: customer._id,
        name: customer.name,
        email: customer.email,
        phone: customer.phone
    };
    
    const movie = await Movie.findById(rental.movieId);
    if(!movie) return res.status(404).send(`Couldn't find a movie with that ID`);
    rental.movie = {
        _id: movie._id,
        title: movie.title,
        dailyRentalRate: movie.dailyRentalRate
    };

    if(movie.numberInStock === 0) return res.status(400).send('Movie not in stock');
    
    try {
        new Fawn.Task()
            .save('rentals', rental)
            .update('movies', { _id: movie._id }, {
                $inc: {
                    numberInStock: -1
                }
            })
            .run();

        res.send(rental);
    } catch(ex) {
        res.status(500);
    }
});

module.exports = router;