const express = require('express');
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');
const { Movie } = require('../models/movie');
const { Rental, validateReturn } = require('../models/rental');
const {differenceInDays} = require('date-fns');

const router = express.Router();


router.put(`/`, [auth, validator(validateReturn)], async (req, res) => {
    let rental = req.body;
    rental = await Rental.lookup(rental.customerId, rental.movieId);
    
    if(!rental) return res.status(404).send(`No rental found for this user and movie.`);
    
    if(rental.dateReturned) return res.status(400).send('Return is already processed');
    
    rental.dateReturned = new Date(); 
    
    const daysRented = differenceInDays(rental.dateReturned, rental.dateCreated);
    rental.rentalFee = daysRented * rental.movie.dailyRentalRate;
    
    rental = await rental.save();

    // and increment the movie stock
    await Movie.findByIdAndUpdate(req.body.movieId, {
        $inc: {
            numberInStock: 1
        }
    }, {
        runValidators: true
    });

    res.send(rental);
});

module.exports = router;



