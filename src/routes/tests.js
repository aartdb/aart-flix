const express = require('express');
const auth = require('../middleware/auth');
const validator = require('../middleware/validator');
const { validate } = require('../models/user');
const admin = require('../middleware/admin');
const validateObjectId = require('../middleware/validateObjectId');

const router = express.Router();

/**
 * These endpoints are made, just for testing
 */
const response = `These endpoints should just be used for testing`;
router.get(`/auth`, auth, async (req, res) => res.send(response));
router.get(`/admin`, [auth, admin], async (req, res) => res.send(response));
router.get(`/validateObjectId/:id`, validateObjectId, async (req, res) => res.send(response));
router.get(`/validator`, validator(validate), async (req, res) => res.send(response));

module.exports = router;



