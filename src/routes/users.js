const debug = require('debug')('app:api:users');
const express = require('express');
const jwt = require('jsonwebtoken');
const { User, validate } = require('../models/user');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const auth = require('../middleware/auth');

const router = express.Router();

// router.get('/', async (req, res) => {
//     try {
//         const users = await User.find();
//         res.send(users);
//     } catch(ex) {
//         res.send(ex.message);
//     }
// });

router.get('/me', auth, async (req, res) => {
    try{
        // get the user from the auth middleware
        const user = await User.findById(req.user._id).select('-password');

        res.send(user);
    } catch(ex) {
        debug(ex.message);
    }
});


router.post('/', async (req, res) => {
    try {
        let user = req.body;
        const { error } = validate(user);
        if(error) return res.status(400).send(new Error(error).message);

        let alreadyRegisteredUser = await User.findOne({ email: user.email })
        if(alreadyRegisteredUser) return res.status(400).send('User already registered.');

        user = new User(user);
        const salt = await bcrypt.genSalt();
        const hashed = await bcrypt.hash(user.password, salt);

        user.password = hashed;
        user = await user.save();
        const token = user.generateAuthToken();
        
        let userInJSON = user.toJSON();
        delete userInJSON.password;
        
        res
            .header('x-auth-token', token)
            .send(userInJSON);
    } catch(ex) {
        res.send(ex.message);
    }
});

module.exports = router;