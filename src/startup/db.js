const mongoose = require('mongoose');
const winston = require('winston');

module.exports = function() {
    const db = process.env.DB;
    mongoose.connect(db)
        .then(() => winston.info(`Connected to MongoDB database: ${db}`));
    
    // make sure persistency is being checked
    function setRunValidators() {
        // only run this if it's not already set
        if (!'runValidators' in this.getOptions()) {
            this.setOptions({ runValidators: true });
        }
    }
    mongoose.plugin(schema => {
        schema.pre('findByIdAndUpdate', setRunValidators); 
    });

}