require('express-async-errors'); // wrap each function in a try-catch block, report the error

const winston = require('winston');
// require('winston-mongodb');

module.exports = function() {
    const logFormat = winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.align(),
        winston.format.printf(
            info => `${info.timestamp} ${info.level}: ${info.message}`
        )
    );

    winston.add(new winston.transports.File({ 
        filename: './logs/logfile.log', // relative to app.js
        handleExceptions: true, 
        format: logFormat 
    }));
    // winston.add(new winston.transports.MongoDB({ 
    //     db: process.env.DB, 
    //     level: 'info', 
    //     handleExceptions: true 
    // }));
    winston.add(new winston.transports.Console({
        handleExceptions: true,
        format: logFormat 
    }));
    
    // get a hold of the uncaught promise rejections and send them to winston's exceptionshandling
    process.on('unhandledRejection', (ex) => {
        throw ex;
    });
}