const morgan = require('morgan');
/**
 * Makes sure all api-calls get logged
 */
module.exports = function(app) {
    if (app.get('env') === 'development') {
        app.use(morgan('tiny'));
    }
}