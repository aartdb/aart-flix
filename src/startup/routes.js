const express = require('express');
const error = require('../middleware/error');
const logger = require('../middleware/logger');
const api = require('../routes/api');

module.exports = function(app) { 
    app.use(express.json());
    app.use(express.static('public')); // make public folder serve static resources
    app.use(logger);
    app.use('/api', api);
    app.use(error);
};