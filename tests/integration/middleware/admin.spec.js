const { execPath } = require('process');
const request = require('supertest');
const { User } = require('../../../src/models/user');

describe(`admin middleware`, () => {
    let server;
    let token;

    beforeEach(async () => {
        server = globalThis.globalServer;    
        token = new User({ isAdmin: true }).generateAuthToken();
    });

    const exec = () => 
        request(server)
            // this endpoint should have the auth middleware, but shouldn't require anything else (like admin-rights)
            .get('/api/tests/admin')
            .set('x-auth-token', token);

    describe(`sad path`, () => {
        it(`should return 403 if user is not an admin`, async () => {
            token = new User({ isAdmin: false }).generateAuthToken();

            const res = await exec();

            expect(res.status).toBe(403);
        })
    })

    describe(`happy path`, () => {
        it(`should return 200 if token is valid`, async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        })
        it(`should add its name to the called middlewares`, async () => {
            globalThis.calledMiddlewares = [];

            const res = await exec();

            expect(globalThis.calledMiddlewares).toContain('admin');
        })
    })

})