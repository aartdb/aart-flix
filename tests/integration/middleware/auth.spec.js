const { execPath } = require('process');
const request = require('supertest');
const { User } = require('../../../src/models/user');

describe(`auth middleware`, () => {
    let server;
    let token;

    beforeEach(async () => {
        server = globalThis.globalServer;    
        token = new User({ isAdmin: false }).generateAuthToken();
    });

    const exec = () => 
        request(server)
            // this endpoint should have the auth middleware, but shouldn't require anything else (like admin-rights)
            .get('/api/users/me')
            .set('x-auth-token', token);

    describe(`sad path`, () => {
        it(`should return 401 if no token is provided`, async () => {
            token = ''; 
            const res = await exec();
 
            expect(res.status).toBe(401);
        })
        it(`should return 400 if token is invalid`, async () => {
            token = 'a';
            const res = await exec();

            expect(res.status).toBe(400);
        })
    })

    describe(`happy path`, () => {
        it(`should return 200 if token is valid`, async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        })
        it(`should add its name to the called middlewares`, async () => {
            globalThis.calledMiddlewares = [];

            const res = await exec();

            expect(globalThis.calledMiddlewares).toContain('auth');
        })
    })

})