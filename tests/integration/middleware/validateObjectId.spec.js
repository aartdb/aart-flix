const mongoose = require('mongoose');
const { execPath } = require('process');
const request = require('supertest');
const { User } = require('../../../src/models/user');

describe(`admin middleware`, () => {
    let server;
    let token;
    let objectId;

    beforeEach(async () => {
        server = globalThis.globalServer;    
        objectId = mongoose.Types.ObjectId();
    });

    const exec = () => 
        request(server)
            // this endpoint should have the auth middleware, but shouldn't require anything else (like admin-rights)
            .get(`/api/tests/validateObjectId/${objectId}`);

    describe(`sad path`, () => {
        it(`should return 404 if not a valid objectId`, async () => {
            objectId = 'invalid';

            const res = await exec();

            expect(res.status).toBe(404);
        })
    })

    describe(`happy path`, () => {
        it(`should return 200 if objectId is valid`, async () => {
            const res = await exec();

            expect(res.status).toBe(200);
        })
        it(`should add its name to the called middlewares`, async () => {
            globalThis.calledMiddlewares = [];

            const res = await exec();

            expect(globalThis.calledMiddlewares).toContain('validateObjectId');
        })
    })

})