const mongoose = require('mongoose');

const { Genre } = require("../../../src/models/genre");
const { User } = require('../../../src/models/user');

const request = require('supertest');
let server;

describe('/api/genres', () => {
    beforeEach(async () => {
        server = globalThis.globalServer;
        await Genre.remove({}); // remove all        
    });
    afterEach(async () => {
        await Genre.remove({}); // remove all
        // await server.close();
    });


    describe('GET /', () => {
        it('should return all genres', async () => {
            const fakeGenres = [
                { name: 'genre1' },
                { name: 'genre2' } 
            ];
            await Genre.collection.insertMany(fakeGenres); 
 
            const res = await request(server).get('/api/genres');

            expect(res.status).toBe(200);

            // now check if the result contains all genres 
            for (const fakeGenre of fakeGenres) {
                expect(res.body).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining({ name: fakeGenre.name })
                    ])
                )
            }
        });
    });

    describe('GET /:id', () => {
        it('should return the genre if it exists', async () => {
            const fakeGenres = [
                { name: 'genre1' },
                { name: 'genre2' }
            ];
            const insertedGenres = (await Genre.collection.insertMany(fakeGenres)).ops;
            const fakeGenre = insertedGenres[0];
            const res = await request(server).get(`/api/genres/${fakeGenre._id}`);

            expect(res.status).toBe(200);
            expect(res.body.name).toEqual(fakeGenre.name);
        });

        it(`should return 404 if invalid id is passed`, async () => {
            const res = await request(server).get(`/api/genres/1`);
            expect(res.status).toBe(404);
        });
        it(`should return 404 if genre with given id doesn't exist`, async () => {
            const id = mongoose.Types.ObjectId(); // generate a new id, doesn't exist in the database
            const res = await request(server).get(`/api/genres/${id}`);
            expect(res.status).toBe(404);
        });
    });

    describe('POST /', () => {
        let token;
        let genreToCreate = { name: '' }; // provide intellisense

        const exec = () =>
            request(server)
                .post(`/api/genres`)
                .set('x-auth-token', token)
                .send(genreToCreate);

        beforeEach(() => {
            token = new User({ name: '12321', isAdmin: true }).generateAuthToken();

            genreToCreate = { name: 'genre1' };
        })

        describe(`sad path`, () => {
            it(`should return 400 if genre is less than 5 characters`, async () => {
                genreToCreate.name = '1234';
                const res = await exec();

                expect(res.status).toBe(400);
            })
            it(`should return 400 if genre is more than 255 characters`, async () => {
                genreToCreate.name = 'a'.repeat(256);
                const res = await exec();

                expect(res.status).toBe(400);
            })
        })

        describe(`happy path`, () => {
            it(`should have called these middlewares`, async () => {
                // reset all called middlewares
                globalThis.calledMiddlewares = [];
            
                await exec();

                const requiredMiddlewares = ['auth', 'admin', 'validator'];
                expect(globalThis.calledMiddlewares).toEqual(expect.arrayContaining(requiredMiddlewares));
            })
            it(`should return the genre in the body`, async () => {
                const res = await exec();

                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty('_id');
                expect(res.body).toEqual(expect.objectContaining(genreToCreate));
            })
            it(`should save the genre`, async () => {
                await exec();

                const createdGenre = await Genre.findOne({ name: genreToCreate.name });
                expect(createdGenre).toEqual(expect.objectContaining(genreToCreate));
            })
        })
    });

    describe('PUT', () => {
        let token;
        let genreToModify; // provide intellisense
        let newName;

        const exec = () =>
            request(server)
                .put(`/api/genres/${genreToModify._id}`)
                .set('x-auth-token', token)
                .send({ name: newName });

        beforeEach(async () => {
            token = new User({ name: '12321', isAdmin: true }).generateAuthToken();
            genreToModify = new Genre({ name: 'Original genre name' });
            await genreToModify.save();
            newName = 'Modified genre name';
        })

        describe(`sad path`, () => {
            it(`should return 401 if client is not logged in`, async () => {
                token = '';
                const res = await exec();

                expect(res.status).toBe(401)
            });
            it(`should return 403 if user is not an admin`, async () => {
                token = new User({ isAdmin: false }).generateAuthToken();
                const res = await exec();

                expect(res.status).toBe(403)
            })
            it(`should return 400 if genre name is less than 5 characters`, async () => {
                newName = '1234';
                const res = await exec();

                expect(res.status).toBe(400);
            })
            it(`should return 400 if genre name is more than 255 characters`, async () => {
                newName = 'a'.repeat(256);
                const res = await exec();

                expect(res.status).toBe(400);
            })
            it(`should return 404 if genre-id is invalid`, async () => {
                genreToModify._id = 'invalid guid';
                const res = await exec();

                expect(res.status).toBe(404);
            })
            it(`should return 404 if genre isn't found`, async () => {
                genreToModify._id = mongoose.Types.ObjectId();
                const res = await exec();

                expect(res.status).toBe(404);
            })
        })

        describe(`happy path`, () => {
            it(`should return the genre in the body`, async () => {
                const res = await exec();

                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty('_id');
                expect(res.body.name).toEqual(newName);
            })
            it(`should save the genre`, async () => {
                await exec();

                const createdGenre = await Genre.findOne({ _id: genreToModify._id });
                expect(createdGenre.name).toEqual(newName);
            })
        })
    })

    describe('DELETE', () => {
        let token;
        let genreToDeleteId;

        const exec = () =>
            request(server)
                .delete(`/api/genres/${genreToDeleteId}`)
                .set('x-auth-token', token);

        beforeEach(async () => {
            token = new User({ name: '12321', isAdmin: true }).generateAuthToken();

            const genreToDelete = new Genre({ name: 'Original genre name' });
            await genreToDelete.save();
            genreToDeleteId = genreToDelete._id;
        })

        describe(`sad path`, () => {
            it(`should return 401 if client is not logged in`, async () => {
                token = '';
                const res = await exec();

                expect(res.status).toBe(401)
            });
            it(`should return 403 if user is not an admin`, async () => {
                token = new User({ isAdmin: false }).generateAuthToken();
                const res = await exec();

                expect(res.status).toBe(403)
            })
            it(`should return 404 if genre-id is invalid`, async () => {
                genreToDeleteId = 'invalid guid';
                const res = await exec();

                expect(res.status).toBe(404);
            })
            it(`should return 404 if genre isn't found`, async () => {
                genreToDeleteId = mongoose.Types.ObjectId();
                const res = await exec();

                expect(res.status).toBe(404);
            })
        })

        describe(`happy path`, () => {
            it(`should return the genre in the body`, async () => {
                const res = await exec();

                expect(res.status).toBe(200);
                // need to add stringifying because we're getting weird errors otherwise
                expect(JSON.stringify([res.body._id])).toBe(JSON.stringify([genreToDeleteId]));
            })
            it(`should have deleted the genre`, async () => {
                // first run it to delete it
                await exec();

                // then try and find it
                const createdGenre = await Genre.findOne({ _id: genreToDeleteId });
                expect(createdGenre).toBeFalsy();
            })
        })
    })
});