const mongoose = require('mongoose');

const { Movie } = require("../../../src/models/movie");
const { User } = require('../../../src/models/user');
const _ = require('lodash')

const request = require('supertest');
const { Genre } = require('../../../src/models/genre');
let server;
let genre;

describe('/api/movies', () => {
    let fakeMovies = [
        { title: '12345', dailyRentalRate: 1, genreId: null },
        { title: '123456', dailyRentalRate: 1, genreId: null },
    ];

    beforeAll(async () => {
        genre = new Genre({
            name: '12345'
        });
        genre = await genre.save();

        fakeMovies = fakeMovies.map(movie => {
            movie.genreId = genre._id;

            return movie;
        });
    })


    beforeEach(async () => {
        server = globalThis.globalServer;
        await Movie.remove({}); // remove all        
    });
    afterEach(async () => {
        await Movie.remove({}); // remove all
        // await server.close();
    });


    describe('GET /', () => {
        it('should return all movies', async () => {
            await Movie.collection.insertMany(fakeMovies);

            const res = await request(server).get('/api/movies');
 
            expect(res.status).toBe(200);

            // now check if the result contains all movies
            for (const fakeMovie of fakeMovies) {
                expect(res.body).toEqual(
                    expect.arrayContaining([
                        expect.objectContaining({ title: fakeMovie.title })
                    ])
                )
            }
        });
    });

    describe('GET /:id', () => {
        let movieId;
        const exec = () => request(server).get(`/api/movies/${movieId}`);

        describe(`sad path`, () => {
            it(`should return 404 if invalid id is passed`, async () => {
                movieId = '1';
    
                const res = await exec();
    
                expect(res.status).toBe(404);
            });
            it(`should return 404 if movie with given id doesn't exist`, async () => {
                movieId = mongoose.Types.ObjectId(); // generate a new id, doesn't exist in the database
    
                const res = await exec();
    
                expect(res.status).toBe(404);
            });
        });

        describe(`happy path`, () => {
            it('should return the movie if it exists', async () => {
                await Movie.collection.insertMany(fakeMovies);
                
                const movieInDb = await Movie.findOne({});
                movieId = movieInDb._id;
                 
                const res = await exec();
                 
                expect(res.status).toBe(200);
                expect(res.body.title).toEqual(movieInDb.title);
            });
        })
    });

    describe('POST /', () => {
        let token;
        let movieToCreate = fakeMovies[0]; // provide intellisense
        
        beforeEach(() => {
            token = new User({ name: '12321', isAdmin: true }).generateAuthToken();
            movieToCreate = _.cloneDeep(fakeMovies[0]);
            delete movieToCreate._id; // just to be sure
        })

        const exec = () =>
            request(server)
                .post(`/api/movies`)
                .set('x-auth-token', token)
                .send(movieToCreate);

        describe(`sad path`, () => {
            it(`should return 400 if movie is less than 5 characters`, async () => {
                movieToCreate.name = '1234';

                const res = await exec();

                expect(res.status).toBe(400);
            })
            it(`should return 400 if movie is more than 255 characters`, async () => {
                movieToCreate.name = 'a'.repeat(256);

                const res = await exec();

                expect(res.status).toBe(400);
            })
            it(`should return 400 if genreId is not valid`, async () => {
                movieToCreate.genreId = '';

                const res = await exec();

                expect(res.status).toBe(400);
            })
            it(`should return 404 if genreId is not a genre`, async () => {
                movieToCreate.genreId = mongoose.Types.ObjectId();
                
                const res = await exec();
                
                expect(res.status).toBe(404); 
            }) 
        })

        describe(`happy path`, () => {
            it(`should have called these middlewares`, async () => {
                // reset all called middlewares
                globalThis.calledMiddlewares = [];
            
                await exec();

                const requiredMiddlewares = ['auth', 'admin', 'validator'];
                expect(globalThis.calledMiddlewares).toEqual(expect.arrayContaining(requiredMiddlewares));
            })
            // it(`should return the movie in the body`, async () => {
            //     const res = await exec();

            //     expect(res.status).toBe(300);
            //     expect(res.body).toHaveProperty('_id');
            //     expect(res.body).toEqual(expect.objectContaining(movieToCreate));
            // })
            // it(`should save the movie`, async () => {
            //     await exec(); 

            //     const createdMovie = await Movie.findOne({ name: movieToCreate.name });
            //     expect(createdMovie).toEqual(expect.objectContaining(movieToCreate));
            // })
        })
    });

    // describe('PUT', () => {
    //     let token;
    //     let movieToModify; // provide intellisense
    //     let newName;

    //     const exec = () =>
    //         request(server)
    //             .put(`/api/movies/${movieToModify._id}`)
    //             .set('x-auth-token', token)
    //             .send({ name: newName });

    //     beforeEach(async () => {
    //         token = new User({ name: '12321', isAdmin: true }).generateAuthToken();
    //         movieToModify = new Movie({ name: 'Original movie name' });
    //         await movieToModify.save();
    //         newName = 'Modified movie name';
    //     })

    //     describe(`sad path`, () => {
    //         it(`should return 401 if client is not logged in`, async () => {
    //             token = '';
    //             const res = await exec();

    //             expect(res.status).toBe(401)
    //         });
    //         it(`should return 403 if user is not an admin`, async () => {
    //             token = new User({ isAdmin: false }).generateAuthToken();
    //             const res = await exec();

    //             expect(res.status).toBe(403)
    //         })
    //         it(`should return 400 if movie name is less than 5 characters`, async () => {
    //             newName = '1234';
    //             const res = await exec();

    //             expect(res.status).toBe(400);
    //         })
    //         it(`should return 400 if movie name is more than 255 characters`, async () => {
    //             newName = 'a'.repeat(256);
    //             const res = await exec();

    //             expect(res.status).toBe(400);
    //         })
    //         it(`should return 404 if movie-id is invalid`, async () => {
    //             movieToModify._id = 'invalid guid';
    //             const res = await exec();

    //             expect(res.status).toBe(404);
    //         })
    //         it(`should return 404 if movie isn't found`, async () => {
    //             movieToModify._id = mongoose.Types.ObjectId();
    //             const res = await exec();

    //             expect(res.status).toBe(404);
    //         })
    //     })

    //     describe(`happy path`, () => {
    //         it(`should return the movie in the body`, async () => {
    //             const res = await exec();

    //             expect(res.status).toBe(200);
    //             expect(res.body).toHaveProperty('_id');
    //             expect(res.body.name).toEqual(newName);
    //         })
    //         it(`should save the movie`, async () => {
    //             await exec();

    //             const createdMovie = await Movie.findOne({ _id: movieToModify._id });
    //             expect(createdMovie.name).toEqual(newName);
    //         })
    //     })
    // })

    // describe('DELETE', () => {
    //     let token;
    //     let movieToDeleteId;

    //     const exec = () =>
    //         request(server)
    //             .delete(`/api/movies/${movieToDeleteId}`)
    //             .set('x-auth-token', token);

    //     beforeEach(async () => {
    //         token = new User({ name: '12321', isAdmin: true }).generateAuthToken();

    //         const movieToDelete = new Movie({ name: 'Original movie name' });
    //         await movieToDelete.save();
    //         movieToDeleteId = movieToDelete._id;
    //     })

    //     describe(`sad path`, () => {
    //         it(`should return 401 if client is not logged in`, async () => {
    //             token = '';
    //             const res = await exec();

    //             expect(res.status).toBe(401)
    //         });
    //         it(`should return 403 if user is not an admin`, async () => {
    //             token = new User({ isAdmin: false }).generateAuthToken();
    //             const res = await exec();

    //             expect(res.status).toBe(403)
    //         })
    //         it(`should return 404 if movie-id is invalid`, async () => {
    //             movieToDeleteId = 'invalid guid';
    //             const res = await exec();

    //             expect(res.status).toBe(404);
    //         })
    //         it(`should return 404 if movie isn't found`, async () => {
    //             movieToDeleteId = mongoose.Types.ObjectId();
    //             const res = await exec();

    //             expect(res.status).toBe(404);
    //         })
    //     })

    //     describe(`happy path`, () => {
    //         it(`should return the movie in the body`, async () => {
    //             const res = await exec();

    //             expect(res.status).toBe(200);
    //             // need to add stringifying because we're getting weird errors otherwise
    //             expect(JSON.stringify([res.body._id])).toBe(JSON.stringify([movieToDeleteId]));
    //         })
    //         it(`should have deleted the movie`, async () => {
    //             // first run it to delete it
    //             await exec();

    //             // then try and find it
    //             const createdMovie = await Movie.findOne({ _id: movieToDeleteId });
    //             expect(createdMovie).toBeFalsy();
    //         })
    //     })
    // })
});