const mongoose = require('mongoose');
const request = require('supertest');
const { User } = require('../../../src/models/user');
const { Movie } = require('../../../src/models/movie');
const {subDays} = require('date-fns');
const { Rental } = require('../../../src/models/rental');

describe('/api/returns', () => {
    let server;
    let payload;
    let rental;
    let movie;
    let token;

    beforeEach(async () => {
        server = globalThis.globalServer;
        payload = {
            customerId: mongoose.Types.ObjectId(),
            movieId: mongoose.Types.ObjectId()
        }
        token = new User({ name: '12321', isAdmin: true }).generateAuthToken();

        movie = new Movie({
            _id: payload.movieId,
            title: '12345',
            dailyRentalRate: 2,
            numberInStock: 10,
            genre: { name: '12345' }
        }); // grab the one we created in the rental
        await movie.save();

        rental = new Rental({
            customer: {
                _id: payload.customerId,
                name: '12345',
                phone: '123456',
            },
            movie: {
                _id: payload.movieId,
                title: '12345',
                dailyRentalRate: 2
            }
        });
        await rental.save();

    });
    afterEach(async () => {
        await Rental.remove({}); // remove all
        await Movie.remove({}); // remove all
        // await server.close();
    });

    const exec = () => request(server)
        .put(`/api/returns`)
        .set('x-auth-token', token)
        .send(payload);

    describe(`sad path`, () => {
        it(`should return 401 if client is not logged in`, async () => {
            token = '';
 
            const res = await exec();
            
            expect(res.status).toBe(401);
        })
        it(`should return 400 if customerId is not provided`, async () => {
            delete payload.customerId;

            const res = await exec();
            
            expect(res.status).toBe(400);
        })
        it(`should return 400 if movieId is not provided`, async () => {
            delete payload.movieId;

            const res = await exec();
            
            expect(res.status).toBe(400);
        })
        it(`should return 404 if no rental found for movie and customer combination`, async () => {
            payload.movieId = mongoose.Types.ObjectId(); // new, not-existing, guid

            const res = await exec();
            
            expect(res.status).toBe(404);
        })
        it(`should return 404 if no rental found for movie and customer combination - part 2`, async () => {
            payload.customerId = mongoose.Types.ObjectId(); // new, not-existing, guid

            const res = await exec();
            
            expect(res.status).toBe(404);
        })
        it(`should return 400 if return is already processed`, async () => {
            rental.dateReturned = new Date();
            await rental.save();
            
            const res = await exec();
            
            expect(res.status).toBe(400);
        })
    })
    describe(`happy path`, () => {
        it(`should have called these middlewares`, async () => {
            // reset all called middlewares
            globalThis.calledMiddlewares = [];
        
            await exec();

            const requiredMiddlewares = ['auth', 'validator'];
            expect(globalThis.calledMiddlewares).toEqual(expect.arrayContaining(requiredMiddlewares));
        })
        it(`should return 200`, async () => {
            const res = await exec();
            
            expect(res.status).toBe(200);
        })
        it(`should return the rental`, async () => {
            const res = await exec();
            
            expect(Object.keys(res.body)).toEqual(
                expect.arrayContaining(['_id', 'dateCreated', 'customer', 'movie', 'rentalFee'])
            );
        })
        it(`should set the returned date on the rental`, async () => {
            await exec();

            const rentalInDb = await Rental.findById(rental._id);
            // make sure it's changed not more than 10 seconds ago
            const diff = new Date() - rentalInDb.dateReturned;
            expect(diff).toBeLessThan(10 * 1000);
        })
        it(`should correctly calculate the rental fee`, async () => {
            const daysRented = 20;
            rental.dateCreated = subDays(new Date(), 20);
            await rental.save();

            await exec();

            const rentalInDb = await Rental.findById(rental._id);
            expect(rentalInDb.rentalFee).toBe(daysRented * movie.dailyRentalRate);
        })
        it(`should increase the stock of the movie`, async () => {
            const prevStock = movie.numberInStock;

            await exec();

            const movieAfter = await Movie.findById(payload.movieId);
            const currentStock = movieAfter.numberInStock;

            expect(currentStock).toBe(prevStock + 1);
        })
    })
    
});