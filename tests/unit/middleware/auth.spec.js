const mongoose = require("mongoose");
const auth = require("../../../src/middleware/auth");
const { User } = require("../../../src/models/user");
 
describe(`auth middleware`, () => {
    it(`should populate req.user with the payload of a valid JWT`, () => {
        const userToCreate = {
            _id: mongoose.Types.ObjectId().toHexString(),
            isAdmin: true
        };
        const token = new User(userToCreate).generateAuthToken();

        const req = {
            header: jest.fn().mockReturnValue(token)
        }
        const res = {};
        const next = jest.fn();

        auth(req, res, next);

        expect(req.user).toMatchObject(userToCreate);
    })
});