const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const { User } = require('../../../src/models/user');

describe(`user.generateAuthToken`, () => {
    it(`should return a valid jwt`, () => {
        const userObj = { 
            _id: new mongoose.Types.ObjectId().toHexString(), 
            isAdmin: true 
        };
        const user = new User(userObj);
        
        const token = user.generateAuthToken();
        const decoded = jwt.verify(token, process.env.JWT_PRIVATE_KEY);

        expect(decoded).toMatchObject(userObj);
    });
});