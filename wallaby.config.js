// Only needed if you use Wallaby - wallabyjs.com
process.env.PORT = 5034; // make sure the testserver runs on a different port than the default tests

module.exports = function (wallaby) {
  return {
    files: [
      '.env.test',
      '!**/*.css',
      'src/**/*.js',
      'app.js',
      'jest.setup.js',
      'jest.runserver.js',
      'jest.config.js',
      'tsconfig.json'
    ],

    tests: [
      'tests/**/*spec.js'
    ],

    env: {
      type: 'node',
      runner: 'node'
    },

    testFramework: 'jest',

    debug: true
  };
};